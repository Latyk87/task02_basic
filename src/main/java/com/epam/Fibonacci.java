
/**
 * Package with small logic programs package-info.java file.
 */
package com.epam;

/**
 * These class contain the methods with calculations
 *
 * @author Borys Latyk
 * @version 1.0-SNAPSHOT
 * @since 09.11.2019
 */
public class Fibonacci {

    public static final int MAX = 100; /**is 100 percent*/
    /**
     * These method is based on Fibonacci formula
     *
     * @author Borys Latyk
     * @version 1.0-SNAPSHOT
     * @since 09.11.2019
     */
    public static void counterFibonacci(int a) {
        int[] fibonnaci = new int[a];

        double percentageofodd = 0;
        double percentageofeven = 0;

        int numFirst = 0;
        int numSecond = 1;
        int numFinal = 0;

        for (int i = 0; i < 2; i++) {
            if (i == 0) {
                fibonnaci[0] = numFirst;
            } else if (i == 1) {
                fibonnaci[1] = numSecond;
            }
        }
        for (int i = 2; i < fibonnaci.length; i++) {
            numSecond = numFirst + numSecond;
            numFirst = numSecond - numFirst;
            numFinal = numFirst + numSecond;
            fibonnaci[i] = numFinal;
        }
        for (int i = 0; i < fibonnaci.length; i++) {
            if (fibonnaci[i] % 2 != 0) {
                percentageofodd++;
            }
            if (fibonnaci[i] % 2 == 0) {
                percentageofeven++;
            }
        }
        System.out.println("Percentage Odd " + percentageofodd / a * MAX);
        System.out.println("Percentage Even " + percentageofeven / a * MAX);
    }
}
