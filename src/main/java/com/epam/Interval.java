
/**
 * Package with small logic programs package-info.java file.
 */
package com.epam;
import java.io.BufferedReader;
import java.io.InputStreamReader;
/**
 * These class contain the logic sequences
 * @since 09.11.2019
 * @author Borys Latyk
 * @version 1.0-SNAPSHOT
 */
public class Interval {
    /**
     * These variables are to calculate total from referred sequences
     * in class Interval
     */
    static int sumOddnumbers = 0;
    static int sumEvennumbers = 0;
    /**
     * These main method is based on simple math sequences
     * @since 09.11.2019
     * @author Borys Latyk
     * @version 1.0-SNAPSHOT
     */

    public static final int INTERVAL = 100; /**value is only for the cycles*/
    /**
     * These main method is based on simple math sequences
     * @since 09.11.2019
     * @author Borys Latyk
     * @version 1.0-SNAPSHOT
     */
    public static void main(String[] args) {
        for (int i = 0; i < INTERVAL; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
                sumOddnumbers += i;
            }
        }
        System.out.println();
        for (int i = INTERVAL; i > 0; i--) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
                sumEvennumbers += i;
            }
        }
        System.out.println();
        System.out.println(sumOddnumbers);
        System.out.println(sumEvennumbers);
        try {
            BufferedReader reader = null;
            reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Please enter the size of Fibonacci set");
            int n = Integer.parseInt(reader.readLine());
            Fibonacci.counterFibonacci(n);
          } catch (Exception e) {
            System.out.println("Exception");
        }
    }
}